package com.example.insertionSort;

import java.util.Random;

class InsertionSort {
    /**
     * złożoność obliczeniowa: O(n^2), gdy dane posortowane O(n)
     * złożoność pamięciowa: Omega(1)
     */
    static void insertionSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int j = i;
            //swap value until it is on correct place (all values before are lower or equal)
            //however index of this value may changed once comparing and moving next values
            while (array[j] < array[j - 1]) {
                int temp = array[j];
                array[j] = array[j - 1];
                array[j - 1] = temp;
                j--;
                if (j == 0) {
                    break;
                }
            }
        }
    }

    /**
     * Return array of random integers
     *
     * @param size   the number of values to generate
     * @param origin the origin (inclusive) of each random value
     * @param bound  the bound (exclusive) of each random value
     */
    static int[] createRandomArray(int size, int origin, int bound) {
        int[] array = new int[size];
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            array[i] = random.ints(1, origin, bound).findFirst().getAsInt();
        }
        return array;
    }
}
