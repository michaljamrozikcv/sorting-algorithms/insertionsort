package com.example.insertionSort;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = InsertionSort.createRandomArray(20, -5, 5);
        System.out.println("Array before sorting:");
        System.out.println(Arrays.toString(array));
        System.out.println("Sorted array:");
        InsertionSort.insertionSort(array);
        System.out.println(Arrays.toString(array));
    }
}
